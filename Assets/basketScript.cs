﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basketScript : MonoBehaviour
{
    public LevelManager lvlManager;
    public bool HasBalls = false;
    Collider m_Collider;
    Vector3 m_Center;
    public Vector3 m_Size, m_Min, m_Max;

    public delegate void OnBallDrop();
    public event OnBallDrop onDrop;

    private void Awake()
    {
        lvlManager = FindObjectOfType<LevelManager>();
        onDrop += lvlManager.checkBasketsForFullness;
        //Fetch the Collider from the GameObject
        m_Collider = GetComponent<Collider>();
        //Fetch the center of the Collider volume
        m_Center = m_Collider.bounds.center;
        //Fetch the size of the Collider volume
        m_Size = m_Collider.bounds.size;
        //Fetch the minimum and maximum bounds of the Collider volume
        m_Min = m_Collider.bounds.min;
        m_Max = m_Collider.bounds.max;
        //Output this data into the console
        //OutputData();
    }
    private void OnTriggerEnter(Collider ball)
    {
        var droppedBall = ball.gameObject.GetComponent<ballScript>();
        if (droppedBall) {          
            HasBalls = true;
            onDrop();
        }
    }
    void OutputData()
    {
        //Output to the console the center and size of the Collider volume
        Debug.Log("Collider Center : " + m_Center);
        Debug.Log("Collider Size : " + m_Size);
        Debug.Log("Collider bound Minimum : " + m_Min);
        Debug.Log("Collider bound Maximum : " + m_Max);
    }
    public void Delete()
    {
        Destroy(gameObject);
    }
}
