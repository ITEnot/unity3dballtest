﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.Collections;
using System.Linq;
using UnityEngine.UI;
public class LevelManager : MonoBehaviour
{
    public basketScript basketPrefab;
    public ballScript ballPrefab;
    public Transform basketParents;
    public Text winText;
    public Camera MainCamera;
    public ballTestUI btsUI;

    [SerializeField]
    public List<ballScript> balls = new List<ballScript>();
    [SerializeField]
    public List<spawnedBaskets> Baskets = new List<spawnedBaskets>();
    [System.Serializable]
    public struct spawnedBaskets
    {
        public basketScript basket;
        //coordinates of transform
        public float xMin;
        public float xMax;

        public spawnedBaskets(basketScript m_basket, float m_xMin, float m_xMax)
        {
            basket = m_basket;
            xMin = m_xMin;
            xMax = m_xMax;
        }
    }
    public float[] accessibleX = new float[2];
    public float[] accessibleY = new float[2];
    void Start()
    {
        basketPrefab.onDrop += checkBasketsForFullness;
        calcLimits();
    }
    private void Update()
    {
        //only one input type, it makes no sense where to handle it
        if (Input.GetMouseButtonDown(0)) {
            //TODO raycast what this isn't UI
            Vector2 clickpos = MainCamera.ScreenToWorldPoint(Input.mousePosition);
            ballScript ball = Instantiate(ballPrefab, clickpos, Quaternion.identity);
            balls.Add(ball);
        }
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) {
                Vector2 touchpos = MainCamera.ScreenToWorldPoint(Input.mousePosition);
                ballScript ball = Instantiate(ballPrefab, touchpos, Quaternion.identity);
                balls.Add(ball);
            }
        }
    }

    public void checkBasketsForFullness()
    {
        if (Baskets.Count != 0) {
            bool result = Baskets.All(basketStruct => basketStruct.basket.HasBalls == true);
            if (result) {
                successCompleteLevel();
            }
        }
    }
    void calcLimits()
    {
        float screenHeightInUnits = Camera.main.orthographicSize * 2;
        float screenWidthInUnits = (screenHeightInUnits * Camera.main.aspect) / 2;
        //calc ranges for don't spawn baskets off screen
        accessibleX[0] = (screenWidthInUnits - basketPrefab.m_Max.x) * -1;
        accessibleX[1] = screenWidthInUnits - basketPrefab.m_Size.x;

        accessibleY[0] = Camera.main.orthographicSize * -1 + basketPrefab.m_Size.x;
        accessibleY[1] = 0;
    }
    public void generateLevel(int size)
    {
        resetLevel();
        generateBaskets(size);
    }

    float xMinPoint;
    float xMaxPoint;
    void generateBaskets(int baskCount)
    {
        for (int i = 0; i < baskCount; i++) {
            int xPoint = UnityEngine.Random.Range((int)accessibleX[0], (int)accessibleX[1]);
            int yPoint = UnityEngine.Random.Range((int)accessibleY[0], (int)accessibleY[1]);

            xMinPoint = xPoint + basketPrefab.m_Min.x;
            xMaxPoint = xPoint + basketPrefab.m_Max.x;
            //check collision
            while (!checkForMathSets(xMinPoint, xMaxPoint, i)) {
                xPoint = UnityEngine.Random.Range((int)accessibleX[0], (int)accessibleX[1]);
                xMinPoint = xPoint + basketPrefab.m_Min.x;
                xMaxPoint = xPoint + basketPrefab.m_Max.x;
            }
            var basket = Instantiate(basketPrefab, new Vector3(xPoint, yPoint, 0), Quaternion.identity);
            var newBasketStruct = new spawnedBaskets(basket, xMinPoint, xMaxPoint);
            Baskets.Add(newBasketStruct);
        }

    }
    bool checkForMathSets(float xMin, float xMax, int index)
    {
        for (int i = 0; i < index; i++) {
            if (Baskets.Count > i) {
                if (RangeHelper.IsInRange(xMin, xMax, Baskets[i].xMin, Baskets[i].xMax))
                    return false;
            }
        }
        return true;
    }
    public void resetLevel()
    {
        if (Baskets.Count != 0) {
            Baskets.ForEach(delegate (spawnedBaskets basketStruct) {
                basketStruct.basket.Delete();
            });
            Baskets.Clear();
        }
        if (balls.Count != 0) {
            balls.ForEach(delegate (ballScript ball) {
                ball.Delete();
            });
            balls.Clear();
        }
    }
    void successCompleteLevel()
    {
        //dumb piece
        btsUI.winScreen();
    }
}
