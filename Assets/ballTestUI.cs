﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballTestUI : MonoBehaviour
{
    public LevelManager lvlManager;
    int lastChosenLevel;
    public Canvas levels;
    public Canvas topBtns;
    public Canvas winCanvas;
    public void restartLevel()
    {
        lvlManager.generateLevel(lastChosenLevel);
    }
    public void home()
    {
        lvlManager.resetLevel();
        levels.gameObject.SetActive(true);
        topBtns.gameObject.SetActive(false);
        winCanvas.gameObject.SetActive(false);
    }
    public void chooseLevel(int quantity)
    {
        lastChosenLevel = quantity;
        lvlManager.generateLevel(quantity);

        levels.gameObject.SetActive(false);
        topBtns.gameObject.SetActive(true);
        winCanvas.gameObject.SetActive(false);
    }
    public void winScreen()
    {
        levels.gameObject.SetActive(false);
        topBtns.gameObject.SetActive(false);
        winCanvas.gameObject.SetActive(true);
    }
}
