﻿using System;
using UnityEngine;


public static class RangeHelper
{
    public static bool IsInRange(float xStart, float xEnd, float start, float end) => xStart >= start && xStart <= end && xEnd >= start && xStart <= end;

}
